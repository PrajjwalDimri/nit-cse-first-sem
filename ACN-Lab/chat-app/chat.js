let app = require("express")();
let server = require("http").Server(app);
let io = require("socket.io")(server);
let request = require("request-promise-native");

server.listen(8080, () => {
  console.log("Listening on port 8080");
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.get("/username", (req, res) => {
  request("https://uinames.com/api/").then(response => {
    response = JSON.parse(response);
    res.json(`${response.name}_${response.region}`);
  });
});

io.on("connection", socket => {
  console.log("Connection activated");
  socket.on("message", data => {
    socket.broadcast.emit("chat", data);
  });
});
