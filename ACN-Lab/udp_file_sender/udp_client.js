var udp = require("dgram");
const client = udp.createSocket("udp4");

var fs = require("fs");
var path = require("path");

let file = fs.createWriteStream(path.join(__dirname, "/client_saturn.png"));

client.on("message", (msg, info) => {
  console.log(msg);
  file.write(msg);
});

client.bind(2222);
