var udp = require("dgram");
var fs = require("fs");
var path = require("path");

var server = udp.createSocket("udp4");

server.on("error", error => {
  console.error(error);
});

const file = fs.readFileSync(path.join(__dirname, "/saturn.png"));
console.log(file);
server.send(file, 2222, "localhost");

server.on("close", () => {
  console.info("Server closed");
});
