var express = require("express");
var app = express();
var http = require("http").createServer(app);
var io = require("socket.io")(http);
var path = require("path");
var fs = require("fs");

app.use(express.static(__dirname + "/public"));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", socket => {
  console.log("A User connected");

  socket.on("video-stream-client-ready", () => {
    socket.emit("video-stream-server-url", "video.mp4");
  });

  socket.on("text-client-ready", () => {
    socket.emit("text-server-emitted", "Text sent through socket from server");
  });

  socket.on("disconnect", () => {
    console.log("A User disconnected");
  });
});

http.listen(3000, () => {
  console.log("Listening on 3000 port");
});
