var net = require('net');
var fs = require('fs');
var path = require('path');

let file = fs.createWriteStream(path.join(__dirname, '/file_client.txt'));

var client = new net.Socket();
client.connect(1337, '127.0.0.1', () => {
    console.log("Connected");
});

client.on('data', (data) => {
    file.write(data);
    client.destroy();
})

client.on('close', () => {
    console.log("Connection closed!");
})