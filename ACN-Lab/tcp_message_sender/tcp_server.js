const net = require("net");
const fs = require("fs");
const path = require("path");

var server = net.createServer();

server.on("connection", socket => {
  const file = fs.readFileSync(path.join(__dirname, "/file_server.txt"));
  socket.write(file);
});

server.listen(1337, "127.0.0.1");
