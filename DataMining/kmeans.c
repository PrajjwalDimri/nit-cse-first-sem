#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float distanceCalculator(int x1, int x2, int y1, int y2)
{
  return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
}

int findmin(float x[])
{
  int cluster = -1;
  int i, small = 999;
  for (int i = 0; i < 3; i++)
  {
    if (x[i] < small)
    {
      small = x[i];
      cluster = i + 1;
    }
  }
  return cluster;
}

void findmean(int x[], int y[], int c[])
{
  int i, c1 = 0, c2 = 0, c3 = 0;
  float meanX[3] = {0, 0, 0}, meanY[3] = {0, 0, 0};
  for (i = 0; i < 10; i++)
  {
    if (c[i] == 1)
    {
      meanX[0] += x[i];
      meanY[0] += y[i];
      c1++;
    }
    else if (c[i] == 2)
    {
      meanX[1] += x[i];
      meanY[1] += y[i];
      c2++;
    }
    else
    {
      meanX[2] += x[i];
      meanY[2] += y[i];
      c3++;
    }
  }
  printf("New Centroids\n");
  printf("(%f ,%f)\t", meanX[0] / c1, meanY[0] / c1);
  printf("(%f ,%f)\t", meanX[1] / c2, meanY[1] / c2);
  printf("(%f ,%f)", meanX[2] / c3, meanY[2] / c3);
}

int main()
{

  int x[] = {2, 2, 8, 5, 7, 6, 1, 6, 9, 4};
  int y[] = {10, 5, 4, 8, 5, 4, 2, 10, 4, 9};

  float x1 = 4, x2 = 7.5, x3 = 2.67;
  float y1 = 9.67, y2 = 4.25, y3 = 5;

  int i = 0, cluster[10];
  float a[3];

  for (i = 0; i < 10; i++)
  {
    a[0] = distanceCalculator(x[i], x1, y[i], y1);
    a[1] = distanceCalculator(x[i], x2, y[i], y2);
    a[2] = distanceCalculator(x[i], x3, y[i], y3);
    cluster[i] = findmin(a);
    printf("%f \t %f \t %f \t %d", a[0], a[1], a[2], cluster[i]);
    printf("\n");
  }
  findmean(x, y, cluster);
  return 0;
}