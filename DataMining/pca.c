#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

#define ATTRIBUTES 10
#define INSTANCES 1024

float data[INSTANCES][ATTRIBUTES];
float meanMatrix[ATTRIBUTES];
float coVarianceMatrix[ATTRIBUTES][ATTRIBUTES];
double coVarianceMatrix1D[ATTRIBUTES * ATTRIBUTES];
float eigenVectors[ATTRIBUTES][ATTRIBUTES];
float transformedMatrix[INSTANCES][ATTRIBUTES] = {0};

float meanRetriever(int columnNumber)
{
  int i = 0;
  float mean = 0;

  for (i = 0; i < INSTANCES; i++)
  {
    mean += data[i][columnNumber];
  }
  return mean / INSTANCES;
}

void meanShifter()
{
  int i = 0, j = 0;
  float mean = 0;

  for (j = 0; j < ATTRIBUTES; j++)
  {
    mean = meanRetriever(j);
    meanMatrix[j] = mean;
    for (i = 0; i < INSTANCES; i++)
    {
      // Mean shifting is done per column
      data[i][j] -= mean;
    }
  }

  printf("\n Mean shifted matrix \n");

  for (i = 0; i < INSTANCES; i++)
  {
    for (j = 0; j < ATTRIBUTES; j++)
      printf("%10.4f", data[i][j]);
    printf("\n");
  }
}

float coVarianceFinder(int column1, int column2)
{
  int i = 0;
  float variance = 0.0f;
  if (column1 == column2)
  {
    for (i = 0; i < INSTANCES; i++)
      variance += (data[i][column1] * data[i][column1]);
    return variance / (INSTANCES - 1);
  }
  else
  {
    for (i = 0; i < INSTANCES; i++)
      variance += (data[i][column1] * data[i][column2]);
    return variance / (INSTANCES - 1);
  }
}

void coVarianceMatrixGenerator()
{
  int i, j, k = 0;
  for (i = 0; i < ATTRIBUTES; i++)
  {
    for (j = 0; j < ATTRIBUTES; j++, k++)
    {
      coVarianceMatrix[i][j] = coVarianceFinder(i, j);
      coVarianceMatrix1D[k] = coVarianceMatrix[i][j];
    }
  }
}

void eigenVectorFinder()
{
  // TODO: Rewrite this without GSL
  // Taken from https://www.gnu.org/software/gsl/doc/html/eigen.html#examples
  gsl_matrix_view m = gsl_matrix_view_array(coVarianceMatrix1D, ATTRIBUTES, ATTRIBUTES);
  gsl_vector *eval = gsl_vector_alloc(ATTRIBUTES);
  gsl_matrix *evec = gsl_matrix_alloc(ATTRIBUTES, ATTRIBUTES);

  gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(ATTRIBUTES);

  gsl_eigen_symmv(&m.matrix, eval, evec, w);
  gsl_eigen_symmv_free(w);

  gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_ASC);

  {
    int i, j;

    for (i = ATTRIBUTES - 1; i >= 0; i--)
    {
      double eval_i = gsl_vector_get(eval, i);
      gsl_vector_view evec_i = gsl_matrix_column(evec, i);

      printf("eigenvalue = %g\n", eval_i);
      printf("eigenvector = \n");

      gsl_vector_fprintf(stdout, &evec_i.vector, "%g");

      for (j = ATTRIBUTES - 1; j >= 0; j--)
        eigenVectors[ATTRIBUTES - (j + 1)][ATTRIBUTES - (i + 1)] = gsl_vector_get(&evec_i.vector, j);
    }
  }

  gsl_vector_free(eval);
  gsl_matrix_free(evec);
}

void dataTransformer()
{
  int i, j, k;
  for (i = 0; i < INSTANCES; i++)
    for (j = 0; j < ATTRIBUTES; j++)
      for (k = 0; k < ATTRIBUTES; k++)
        transformedMatrix[i][j] += data[i][k] * eigenVectors[k][j];

  printf("\n Transformed Matrix: \n");
  for (i = 0; i < INSTANCES; i++)
  {
    for (j = 0; j < ATTRIBUTES; j++)
      printf("%f\t", transformedMatrix[i][j]);
    printf("\n");
  }
}

int main(void)
{
  FILE *fp;
  int i, j;
  fp = fopen("cloud.data", "r");

  if (fp == NULL)
  {
    fprintf(stderr, "Can't open the file");
    exit(1);
  }

  // Reading data from the file
  for (i = 0; i < INSTANCES; i++)
    for (j = 0; j < ATTRIBUTES; j++)
      fscanf(fp, "%f", &data[i][j]);

  meanShifter();
  coVarianceMatrixGenerator();
  eigenVectorFinder();
  dataTransformer();

  fclose(fp);
  return 0;
}