const { mergeSort } = require("./mergeSort");

class Point {
  constructor(x, y, polarAngle) {
    this.x = x;
    this.y = y;
    this.polarAngle = polarAngle;
  }
}

let points = [
  new Point(0, 3, 0),
  new Point(1, 1, 0),
  new Point(2, 2, 0),
  new Point(4, 4, 0),
  new Point(0, 0, 0),
  new Point(1, 2, 0),
  new Point(3, 1, 0),
  new Point(3, 3, 0)
];

// Select the point with least y coordinate
let leastPointIndex = 0;
for (let i = 0; i < points.length; i++) {
  if (points[i].y < points[leastPointIndex].y) {
    leastPointIndex = i;
  } else if (points[i].y === points[leastPointIndex].y) {
    if (points[i].x < points[leastPointIndex].x) {
      leastPointIndex = i;
    }
  }
}

// Swap the first point with the point with least y coordinate
let temp = points[0];
points[0] = points[leastPointIndex];
points[leastPointIndex] = temp;

// Calculate polar angles
for (let i = 1; i < points.length; i++) {
  points[i].polarAngle = Math.atan(
    (points[i].y - points[0].y) / (points[i].x - points[0].y)
  );
}

// Sort points on basis of polar angles
points = mergeSort(points);

// Check if two or more points have the same angle. Keep the farthest one.
for (let i = 1; i < points.length - 1; i++) {
  if (points[i].polarAngle === points[i + 1].polarAngle) {
    let firstPointDistance = Math.sqrt(
      Math.pow(points[i].x - points[0].x, 2) +
        Math.pow(points[i].y - points[0].y, 2)
    );
    let secondPointsDistance = Math.sqrt(
      Math.pow(points[i + 1].x - points[0].x, 2) +
        Math.pow(points[i + 1].y - points[0].y, 2)
    );

    if (firstPointDistance < secondPointsDistance) {
      points.splice(i, 1);
    } else {
      points.splice(i + 1, 1);
    }
    i = 1;
  }
}

function clockWiseOrientation(p, q, r) {
  let val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
  if (val === 0) return 0;
  return val > 0 ? 1 : 2;
}

let stack = [];

stack.push(points[0]);
stack.push(points[1]);
stack.push(points[2]);

for (let i = 3; i < points.length; i++) {
  while (
    clockWiseOrientation(
      stack[stack.length - 2],
      stack[stack.length - 1],
      points[i]
    ) !== 2
  ) {
    stack.pop();
  }
  stack.push(points[i]);
}

console.log(stack);
