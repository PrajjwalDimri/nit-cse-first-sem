#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int graph[100][100] = {0};
int numberOfVertices = 0;
int spanningTreeIndex1[100] = {88888};
int spanningTreeIndex2[100] = {88888};
int spanningTreeCurrentCount = 0;

int unionFind(int newEdge1, int newEdge2)
{
  int i, j;
  for (i = 0; i < spanningTreeCurrentCount; i++)
  {
    if (newEdge2 == spanningTreeIndex1[i])
    {
      return 1;
    }
  }  

  return 0;
}

int main()
{
  int i, j, vertex;
  printf("Enter the number of vertices \t");
  scanf("%d", &numberOfVertices);

  // Entering the graph
  for (i = 0; i < numberOfVertices; i++)
  {
    for (j = 0; j < numberOfVertices; j++)
    {
      if (i != j && i < j)
      {
        printf("Enter the edge weight of %d-%d \t", i, j);
        scanf("%d", &graph[i][j]);
        graph[j][i] = graph[i][j];
      }
      else if (i == j)
      {
        graph[i][i] = 99999;
      }
    }
  }

  // Selecting an arbitrary vertex
  vertex = 2;

  printf("**** %d ****", vertex);

  for (j = 1; j < numberOfVertices; j++)
  {
    int lowestEdgeIndex1 = vertex;
    int lowestEdgeIndex2 = 0;
    for (i = 1; i < numberOfVertices; i++)
    {
      if (graph[vertex][i] < graph[lowestEdgeIndex1][lowestEdgeIndex2] && graph[vertex][i] > 0)
      {
        if (unionFind(vertex, i) > 0)
        {
          graph[vertex][i] = 99999;
          graph[i][vertex] = 99999;
          i = 0;
          continue;
        }
        lowestEdgeIndex1 = vertex;
        lowestEdgeIndex2 = i;
      }
    }
    spanningTreeIndex1[spanningTreeCurrentCount] = vertex;
    spanningTreeIndex2[spanningTreeCurrentCount] = lowestEdgeIndex2;
    graph[vertex][lowestEdgeIndex2] = 99999;
    graph[lowestEdgeIndex2][vertex] = 99999;
    spanningTreeCurrentCount++;
    vertex = lowestEdgeIndex2;
  }

  for (i = 0; i < spanningTreeCurrentCount; i++)
  {
    printf("\n Edge [%d] ---- [%d]", spanningTreeIndex1[i], spanningTreeIndex2[i]);
  }
}