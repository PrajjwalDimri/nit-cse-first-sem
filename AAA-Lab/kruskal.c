#include <stdio.h>
#include <stdlib.h>

int graph[100][100] = {0};
int numberOfVertices = 0;
int lowestIndex1 = 0, lowestIndex2 = 1;
int spanningTreeIndex1[100] = {88888};
int spanningTreeIndex2[100] = {88888};
int spanningTreeCurrentCount = 0;

void calculateLowestIndexes()
{
  int i, j;
  for (i = 0; i < numberOfVertices; i++)
  {
    for (j = i + 1; j < numberOfVertices; j++)
    {
      if (graph[i][j] > 0 && graph[lowestIndex1][lowestIndex2] > graph[i][j])
      {
        lowestIndex1 = i;
        lowestIndex2 = j;
      }
    }
  }
  graph[lowestIndex1][lowestIndex2] = 99999;
}

int unionFind(int newEdge1, int newEdge2)
{
  int i, j;
  for (i = 0; i < spanningTreeCurrentCount; i++)
  {
    if (newEdge2 == spanningTreeIndex1[i])
    {
      return 1;
    }
  }

  for (j = 0; j < spanningTreeCurrentCount; j++)
  {
    if (newEdge1 == spanningTreeIndex2[j])
    {
      return 1;
    }
  }

  return 0;
}

int main()
{
  int i, j;
  printf("Enter the number of vertices \t");
  scanf("%d", &numberOfVertices);

  // Entering the graph
  for (i = 0; i < numberOfVertices; i++)
  {
    for (j = 0; j < numberOfVertices; j++)
    {
      if (i != j && graph[i][j] != 99999)
      {
        printf("Enter the edge weight of %d-%d \t", i, j);
        scanf("%d", &graph[i][j]);
        graph[j][i] = 99999;
      }
    }
  }

  // Minimum edge finder
  for (i = 0; i < numberOfVertices; i++)
  {
    calculateLowestIndexes();
    if (spanningTreeCurrentCount == 0)
    {
      spanningTreeIndex1[spanningTreeCurrentCount] = lowestIndex1;
      spanningTreeIndex2[spanningTreeCurrentCount] = lowestIndex2;
      spanningTreeCurrentCount++;
    }
    else
    {
      // Check with union-find if this forms a cycle or not
      if (unionFind(lowestIndex1, lowestIndex2) == 0)
      {
        spanningTreeIndex1[spanningTreeCurrentCount] = lowestIndex1;
        spanningTreeIndex2[spanningTreeCurrentCount] = lowestIndex2;
        spanningTreeCurrentCount++;
      }
    }
  }

  for (i = 0; i < spanningTreeCurrentCount; i++)
  {
    printf("\n Edge [%d] ---- [%d]", spanningTreeIndex1[i], spanningTreeIndex2[i]);
  }
}