#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{

  char text[] = "It is pushing the envelope At the end of the FirstSpriti Digital Experience Platform. Helping marketers serve unmatched cross-phase personalized experiences at every step of the FirstSpriti Digital Experience Platform powers enterprise-class. Spriti introduced new capabilities to the awards page of the FirstSpriti Digital Experience Platform powers enterprise-class. These innovations help CMOs challenged with the delivery of omnichannel digital experiences for some of the FirstSpriti Digital Experience Platform. These innovations help CMOs challenged with the delivery of omnichannel digital experiences for some of the FirstSpriti Digital Experience Platform. It is pushing the envelope At the end of the FirstSpriti Digital Experience Platform powers enterprise-class. Clicking on this link which refers to B2B Marketing awards shortlist will take you to the awards page of the customer journey. Helping marketers serve unmatched cross-phase personalized experiences at every step of the FirstSpriti Digital Experience Platform powers enterprise-class. Clicking on this link which refers to B2B Marketing awards shortlist will take you to the awards page of the customer journey. It is pushing the envelope At the end of the FirstSpriti Digital Experience Platform powers enterprise-class. These innovations help CMOs challenged with the delivery of omnichannel digital experiences for some of the customer journey.";

  char pattern[30];
  int patternLength, textLength, i, j;
  puts("Enter a pattern to match");
  fgets(pattern, 30, stdin);
  // WARN: -1 because fgets also counts enter as a character so the length provided is +1 than intended.
  patternLength = strlen(pattern) - 1;
  textLength = strlen(text);

  int prefixSuffixTable[patternLength];
  prefixSuffixTable[0] = 0;

  // Populate Prefix Suffix Table
  for (i = 0, j = 1; j <= patternLength; j++)
  {
    if (pattern[i] == pattern[j])
    {
      prefixSuffixTable[j] = i + 1;
      i++;
    }
    else
    {
      prefixSuffixTable[j] = 0;
    }
  }

  // Matching algorithm
  for (i = 0, j = 0; i < textLength;)
  {
    if (pattern[j] == text[i])
    {
      if (j >= patternLength - 1)
      {
        printf("Match found at index %d \n", i);
        j = prefixSuffixTable[j - 1];
      }
      j++;
      i++;
    }
    else
    {
      i++;
    }
  }
}