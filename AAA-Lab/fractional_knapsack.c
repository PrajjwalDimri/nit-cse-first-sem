#include <stdio.h>

#define ELEMENTS 3

int weights[] = {25,24,15};
int profits[] = {18,15,10};

float profitArray[ELEMENTS];

void swap(int i, int j)
{
  float temp;
  temp = profitArray[i];
  profitArray[i] = profitArray[j];
  profitArray[j] = temp;

  temp = weights[i];
  weights[i] = weights[j];
  weights[j] = temp;

  temp = profits[i];
  profits[i] = profits[j];
  profits[j] = temp;
}

int main()
{
  float knapsackWeight = 50;
  float currentWeight = 0, remainingWeight;
  int i, j;

  for (i = 0; i < ELEMENTS; i++)
  {
    profitArray[i] = (float)profits[i] / (float)weights[i];
  }

  // Sort the profit array
  for (i = 0; i < ELEMENTS; i++)
  {
    for (j = i + 1; j < ELEMENTS; j++)
    {
      if (profitArray[j] > profitArray[i])
      {
        swap(i, j);
      }
    }
  }

  printf("\n %f \n", profitArray[1]); 

  for (i = 0; i < ELEMENTS; i++)
  {
    if (currentWeight + weights[i] <= knapsackWeight)
    {
      currentWeight = currentWeight + weights[i];
      printf("\n Added item with weight %d and profit %d completely", weights[i], profits[i]);
    }
    else
    {
      // Stop knapsack
      remainingWeight = knapsackWeight - currentWeight;
      printf("\n %f \n", profitArray[i]);
      printf("\n Added %f of item with weight %d and profit %d", remainingWeight * profitArray[i], weights[i], profits[i]);
      i = ELEMENTS;
    }
  }

  return 0;
}
