#include <stdio.h>
#include <stdlib.h>

int gcdEuclid(int a, int b)
{
  int mod;
  if (a == 0)
  {
    return b;
  }
  else if (b == 0)
  {
    return a;
  }

  mod = a % b;
  gcdEuclid(b, mod);
}

int main()
{
  int firstNumber, secondNumber;
  printf("\n Enter bigger number \t");
  scanf("%d", &firstNumber);
  printf("\n Enter smaller number \t");
  scanf("%d", &secondNumber);

  printf("\n  **** Gcd is %d  ****\n", gcdEuclid(firstNumber, secondNumber));
  return 0;
}