#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define ELEMENTS 5
#define CAPACITY 50

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

int valuesArray[ELEMENTS][CAPACITY];
int weights[ELEMENTS + 1] = {0, 40, 5, 6, 4, 3};
int values[ELEMENTS + 1] = {0, 7, 6, 3, 7, 1};

int result;

int knapsack(int n, int w)
{
  if (valuesArray[n][w] != -1)
    return valuesArray[n][w];

  if (n == 0 || w == 0)
    result = 0;

  else if (weights[n] > w)
    result = knapsack(n - 1, w);

  else
  {
    int temp1 = knapsack(n - 1, w);
    int temp2 = values[n] + knapsack(n - 1, w - weights[n]);
    result = MAX(temp1, temp2);
  }

  valuesArray[n][w] = result;
  return result;
}

int main()
{
  int i, j;
  for (i = 0; i <= ELEMENTS; i++)
    for (j = 0; j <= CAPACITY; j++)
      valuesArray[i][j] = -1;

  printf("%d\n", knapsack(ELEMENTS, CAPACITY));
  return 0;
}