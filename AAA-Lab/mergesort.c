#include <stdio.h>
#include <stdlib.h>

#define INPUT_SIZE 10

void merge(int *array, int *left, int leftCount, int *right, int rightCount)
{
  int i = 0, j = 0, k = 0;
  while (i < leftCount && j < rightCount)
  {
    if (left[i] < right[j])
      array[k++] = left[i++];
    else
    {
      array[k++] = right[j++];
    }
  }
  while (i < leftCount)
    array[k++] = left[i++];
  while (j < rightCount)
    array[k++] = right[j++];
}

void mergeSort(int *array, int length)
{
  int mid, i, *Left, *Right;
  if (length < 2)
    return;
  mid = length / 2;

  Left = (int *)malloc(mid * sizeof(int));
  Right = (int *)malloc((length - mid) * sizeof(int));

  for (i = 0; i < mid; i++)
    Left[i] = array[i];

  for (i = mid; i < length; i++)
    Right[i - mid] = array[i];

  mergeSort(Left, mid);
  mergeSort(Right, length - mid);
  merge(array, Left, mid, Right, length - mid);
  free(Left);
  free(Right);
}

int main()
{
  int inputArray[INPUT_SIZE] = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, i;
  mergeSort(inputArray, INPUT_SIZE);

  for (i = 0; i < INPUT_SIZE; i++)
    printf("%d\t", inputArray[i]);
}