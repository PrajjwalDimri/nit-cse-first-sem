class Graph {
  constructor() {
    this.adjacencyList = {};
  }

  addVertex(vertex) {
    this.adjacencyList[vertex] = [];
  }

  addEdge(vertex1, vertex2) {
    this.adjacencyList[vertex1].push(vertex2);
    this.adjacencyList[vertex2].push(vertex1);
  }

  bfs() {
    const nodes = Object.keys(this.adjacencyList);
    let visited = {};
    let queue = [];
    queue.push(nodes[0]);
    while (queue.length > 0) {
      if (!visited[queue[0]]) {
        visited[queue[0]] = true;
        console.log(queue[0]);
        Array.prototype.push.apply(queue, this.adjacencyList[queue[0]]);
      }
      queue.shift();
    }
  }

  dfs() {
    const nodes = Object.keys(this.adjacencyList);
    let visited = {};
    let stack = [];
    stack.push(nodes[0]);
    while (stack.length > 0) {
      if (!visited[stack[stack.length - 1]]) {
        visited[stack[stack.length - 1]] = true;
        console.log(stack[stack.length - 1]);
        const popped = stack.pop();
        Array.prototype.push.apply(stack, this.adjacencyList[popped]);
      } else {
        stack.pop();
      }
    }
  }
}

const graph = new Graph();
graph.addVertex("a");
graph.addVertex("b");
graph.addVertex("c");
graph.addVertex("d");
graph.addVertex("e");
graph.addVertex("f");

graph.addEdge("a", "b");
graph.addEdge("a", "c");
graph.addEdge("a", "d");
graph.addEdge("b", "e");
graph.addEdge("c", "e");
graph.addEdge("d", "f");
graph.addEdge("e", "f");

graph.bfs();
graph.dfs();
