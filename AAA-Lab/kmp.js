const fs = require("fs");
const path = require("path");

let text = fs.readFileSync(path.join(__dirname, "textFile.txt")).toString();

function naiveSearch(pattern, text) {
  let result = [];
  let isMatched = false;
  for (let i = 0; i < text.length - pattern.length + 1; i++) {
    for (let j = 0; j < pattern.length; j++) {
      if (pattern[j] !== text[i + j]) {
        isMatched = false;
        j = pattern.length;
      } else {
        isMatched = true;
      }
    }
    if (isMatched) result.push(i);
  }
  return result;
}

console.time("Naive");
console.info("Running Naive Algorithm with complexity O(mn)");
console.log("Results found at indexes: ", naiveSearch("that", text));
console.timeEnd("Naive");

let prefixSuffixArray = [];

function prefixSuffixArrayGenerator(pattern) {
  let i = 0,
    j = 1;
  prefixSuffixArray = new Array(pattern.length).fill(0);
  let prefixSuffixValue = 0;
  prefixSuffixArray[0] = 0;
  while (j < pattern.length) {
    if (pattern[i] === pattern[j]) {
      prefixSuffixValue++;
      prefixSuffixArray[j] = prefixSuffixValue;
      i++;
      j++;
    } else if (pattern[i] !== pattern[j] && i !== 0) {
      // Reset i to previous prefixSuffix value if the strings don't match at that index
      i = prefixSuffixArray[i - 1];
      prefixSuffixValue = i;
    } else {
      prefixSuffixArray[j] = 0;
      prefixSuffixValue = 0;
      j++;
    }
  }
}

function kmp(pattern, text) {
  prefixSuffixArrayGenerator(pattern);
  let i = 0,
    j = 0;
  while (i < text.length) {
    if (text[i] !== pattern[j]) {
      if (j === 0) {
        i += 1;
      } else {
        j = prefixSuffixArray[j - 1];
      }
    } else {
      i++;
      j++;
      if (j >= pattern.length) {
        console.log("Pattern found at index ", i - j);
        j = prefixSuffixArray[j - 1];
      }
    }
  }
}

console.time("Kmp");
kmp("that", text);
console.timeEnd("Kmp");
