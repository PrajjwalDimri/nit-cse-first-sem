// const readline = require('readline').createInterface({
//     input: process.stdin,
//     output: process.stdout
// });

let inputString = "SA-%^$2ADSADAWEQWEAASDAADADA";
let inputPattern = "ADA";

// readline.question("Enter the input string: ", (string) => {
//     inputString = string;
//     readline.question("Enter the input pattern: ", (pattern) => {
//         inputPattern = pattern;
//         readline.close();


//     });
// });

let prime = 13;
let patternHash = 0,
    inputHash = 0;
for (let i = 0; i < inputPattern.length; i++) {
    patternHash += inputPattern[i].charCodeAt(0) * Math.pow(prime, i);
}
console.log("Hash of the pattern is : ", patternHash);

for (let i = 0; i < inputPattern.length; i++) {
    inputHash += inputString[i].charCodeAt(0) * Math.pow(prime, i);
}

if (inputHash === patternHash) {
    console.log("Found at index 0");
}

for (let j = 1; j <= inputString.length - inputPattern.length; j++) {
    inputHash -= inputString[j - 1].charCodeAt(0);
    inputHash /= prime;
    inputHash += inputString[j + inputPattern.length - 1].charCodeAt(0) * Math.pow(prime, inputPattern.length - 1);

    if (inputHash === patternHash) {
        console.log("Found at index ", j);
    }
}