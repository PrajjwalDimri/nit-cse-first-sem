#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int hasher(char *string)
{
  int i, j;
  int length = strlen(string);
  int hash = 0;
  j = length - 1;

  for (int i = 0; i < length; i++, j--)
    hash += pow(3, j) * ((int)string[i]);

  return hash;
}

int main()
{
  char inputString[] = "aataatksldjhasoiue   ya aatasoieudas98^*&^%^&*%aat";
  char *pattern;
  int patternHash = 0, i, inputLength, patternLength = 0, currentHash = 0, occurences = 0;
  float prevHash = 0.0f;

  inputLength = strlen(inputString);
  fputs("Enter a pattern to search (Max 15 length) \n", stdout);
  fgets(pattern, 15, stdin);
  patternLength = strlen(pattern);
  // fgets is also reading the enter as a character and changint the value.
  pattern[patternLength - 1] = '\0';
  patternLength = strlen(pattern);
  patternHash = hasher(pattern);

  printf("PatternHash:%d\n", patternHash);

  for (i = 0; i < inputLength; i++)
  {
    if (i == 0)
    {
      char string[15];
      strncpy(string, inputString, patternLength);
      string[patternLength] = '\0';
      prevHash += hasher(string);
      i = patternLength - 1;
    }
    else
    {
      if (prevHash == patternHash)
        occurences += 1;
      prevHash = ((prevHash - ((int)inputString[i - patternLength]) * pow(3, patternLength - 1)) * 3) + (int)inputString[i];
    }
  }

  printf("String occured %d times\n", occurences);
}