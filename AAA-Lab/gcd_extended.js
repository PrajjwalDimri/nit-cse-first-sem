function extended_gcd(a,b){
    let x = 0, y = 1, u = 1, v = 0;
    
    while(a!==0){
        let quotient = Math.floor(b/a);
        let remainder = b%a;
        let m = x-u*quotient;
        let n = y-v*quotient;
        b = a;
        a=remainder;
        x=u;
        y=v;
        u=m;
        v=n;
    }
    console.log("GCD", b);
    console.log("Quotients by the GCD:", x, y);
}

extended_gcd(81,57)